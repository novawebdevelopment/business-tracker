from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from common import views

urlpatterns = [
    path("accounts/", include("apps.accounts.urls")),
    path("invoices/", include("apps.invoices.urls")),
    path("partners/", include("apps.partners.urls")),
    path("projects/", include("apps.projects.urls")),
    path("servers/", include("apps.servers.urls")),
    path("settings/", views.SettingsView.as_view(), name="settings"),
    path("settings/dump/json/", views.DumpJsonView.as_view(), name="dump_json"),
    path("settings/dump/xml/", views.DumpXmlView.as_view(), name="dump_xml"),
    path("settings/reset/invoice/template/", views.ResetInvoiceTemplateView.as_view(), name="reset_invoice_template"),
    path("", login_required(TemplateView.as_view(template_name="dashboard.html")), name="dashboard"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
