from configparser import ConfigParser
from django import forms
from django.conf import settings


class SettingsForm(forms.Form):
    invoice_template = forms.FileField(
        required=False, label="Invoice Template", help_text="Can be left empty to keep the current template"
    )
    email_host = forms.CharField(required=False, label="Email Host", help_text="E.g. mail.novawebdevelopment.org")
    email_port = forms.IntegerField(label="Email Port")
    email_user = forms.CharField(
        required=False, label="Email User", help_text="E.g. do-not-reply@novawebdevelopment.org"
    )
    email_password = forms.CharField(required=False, label="Email Password")
    email_use_tls = forms.BooleanField(required=False, label="Use TLS for Email")
    weekly_hours = forms.IntegerField(label="Weekly Hours", help_text="Number of hours that should be tracked per week")
    overdue_days = forms.IntegerField(
        label="Overdue Days",
        help_text="Number of days after which a billed invoice is marked OVERDUE (0 = never)",
    )
    notice = forms.CharField(
        label="Notice",
        help_text="Displayed at the bottom of the invoice. Long text might be cut out depending on the invoice template",
        widget=forms.Textarea,
    )
    billing_message = forms.CharField(
        label="Billing Message",
        help_text="[contact_name] is replaced with the partner's contact name",
        widget=forms.Textarea,
    )
    auto_email = forms.BooleanField(
        required=False,
        label="Email Invoices to Partners",
        help_text="Automatically email invoices to partners when billing them",
    )
    linode_api_token = forms.CharField(required=False, label="Linode API Token", help_text="Can be left empty")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Read the config.ini file
        self.config = ConfigParser(interpolation=None)
        self.config.read(settings.CONFIG_FILE)

        # Set initial values from the config file
        self.initial["email_host"] = self.config.get("email", "EMAIL_HOST")
        self.initial["email_port"] = self.config.getint("email", "EMAIL_PORT")
        self.initial["email_user"] = self.config.get("email", "EMAIL_USER")
        self.initial["email_password"] = self.config.get("email", "EMAIL_PASSWORD")
        self.initial["email_use_tls"] = self.config.getboolean("email", "EMAIL_USE_TLS")
        self.initial["weekly_hours"] = self.config.getint("accounts", "WEEKLY_HOURS")
        self.initial["overdue_days"] = self.config.getint("invoices", "OVERDUE_DAYS")
        self.initial["notice"] = self.config.get("invoices", "NOTICE")
        self.initial["billing_message"] = self.config.get("invoices", "BILLING_MESSAGE")
        self.initial["auto_email"] = self.config.getboolean("invoices", "AUTO_EMAIL")
        self.initial["linode_api_token"] = self.config.get("servers", "LINODE_API_TOKEN")

    def clean_email_port(self):
        if not self.cleaned_data["email_port"] in range(0, 65535):
            raise forms.ValidationError("Enter an integer between 0 and 65535.")
        return self.cleaned_data["email_port"]

    def clean_weekly_hours(self):
        if self.cleaned_data["weekly_hours"] < 0:
            raise forms.ValidationError("Enter a positive integer.")
        return self.cleaned_data["weekly_hours"]

    def clean_overdue_days(self):
        if self.cleaned_data["overdue_days"] < 0:
            raise forms.ValidationError("Enter a positive integer.")
        return self.cleaned_data["overdue_days"]

    def save(self):
        self.config.set("email", "EMAIL_HOST", str(self.cleaned_data["email_host"]))
        self.config.set("email", "EMAIL_PORT", str(self.cleaned_data["email_port"]))
        self.config.set("email", "EMAIL_USER", str(self.cleaned_data["email_user"]))
        self.config.set("email", "EMAIL_PASSWORD", str(self.cleaned_data["email_password"]))
        self.config.set("email", "EMAIL_USE_TLS", str(self.cleaned_data["email_use_tls"]))
        self.config.set("accounts", "WEEKLY_HOURS", str(self.cleaned_data["weekly_hours"]))
        self.config.set("invoices", "OVERDUE_DAYS", str(self.cleaned_data["overdue_days"]))
        self.config.set("invoices", "NOTICE", str(self.cleaned_data["notice"]))
        self.config.set("invoices", "BILLING_MESSAGE", str(self.cleaned_data["billing_message"]))
        self.config.set("invoices", "AUTO_EMAIL", str(self.cleaned_data["auto_email"]))
        self.config.set("servers", "LINODE_API_TOKEN", str(self.cleaned_data["linode_api_token"]))

        with open(settings.CONFIG_FILE, "w") as f:
            self.config.write(f, space_around_delimiters=False)
