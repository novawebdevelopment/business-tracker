$(document).ready(function () {
  $('.sidenav > div > a[href="' + window.location.pathname + '"]').addClass('active');
  $('.sidenav > div > a[href="' + window.location.pathname + window.location.search + '"]').addClass('active');
  setTimeout(function () {
    $(".alert-dismissible").alert("close");
  }, 5000);
  table = $('#jsTable').DataTable({
    responsive: true,
    dom: '<"row no-gutters flex-nowrap"<"flex-grow-1"f><"flex-shrink-0 ml-1"l>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    lengthMenu: [5, 10, 25, 50, 100],
    pagingType: "numbers",
    pageLength: 10,
    language: {
      lengthMenu: "_MENU_",
      search: "_INPUT_",
      searchPlaceholder: "Search"
    },
    columnDefs: [{
      targets: [-1],
      orderable: false
    },
    {
      targets: [0, -1],
      responsivePriority: 1
    }]
  });

  var rootPassword = $("#rootPassword").text();
  $("#rootPassword").text(rootPassword.replace(/./g, '*'));

  $("#rosModal").modal('show');

  $('#ipAddressButton').click(function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('#ipAddress').text()).select();
    document.execCommand("copy");
    $temp.remove();
  });
  $('#rootPasswordButton').click(function () {
    var $temp = $("#rootPassword");
    if ($temp.text() === rootPassword) {
      $temp.text(rootPassword.replace(/./g, '*'));
    }
    else {
      $temp.text(rootPassword);
    }
  });
  $('#consoleButton').click(function (e) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(rootPassword).select();
    document.execCommand("copy");
    $temp.remove();
    if (!confirm('The root password has been copied to your clipboard.\nAfter pressing OK, you will be redirected to the Linode in-browser terminal.\nPaste the password when asked using CTRL-SHIFT-V.')) {
      e.preventDefault();
    }
  });

  $("#emailPasswordButton").click(function () {
    if ($(this).hasClass("active")) {
      $("#id_email_password").attr("type", "password");
    }
    else {
      $("#id_email_password").attr("type", "text");
    }
  });

  $('input[name*="partner_id"]').on("click", function () {
    if (confirm("You should leave this field empty unless it's absolutely necessary to manually provide the Partner ID.\n\nAre you sure you want to continue?")) {
      $('input[name*="partner_id"]').focus()
    }
    else {
      $('input[name*="company"]').focus()
      $('input[name*="partner_id"]').val("");
    }
  });

  $('.advance-invoice').on("click", function () {
    return confirm("You are going to advance the status of the invoice.\nThis action cannot be undone.\n\nAre you sure you want to continue?");
  });

  $('#jsTable_length').removeClass('dataTables_length');
  $('#jsTable_filter').removeClass('dataTables_filter');
  $('#jsTable_filter > label').contents().unwrap();
  $('#jsTable_length > label').contents().unwrap();
  $('#jsTable_filter > input').removeClass("form-control-sm");
  $('#jsTable_length > select').removeClass("custom-select-sm form-control-sm");

  $('input[name*="rate"]').on("input", updateItem);
  $('input[name*="hours"]').on("input", updateItem);
  $('input[name*="amount"]').on("input", updateItem);

  $('#id_items_table tbody').each(function (e) {
    $(this).children('tr').each(function (e) {
      $(this).children('td').each(function (e) {
        $(this).children('input').each(function (e) {
          $(this).trigger("input");
        })
      })
    })
  })

  $('#invoiceForm').submit(function (e) {
    $('input[name*="items-"]:disabled').each(function (e) {
      $(this).removeAttr('disabled');
    });
  });

  $('#serverForm').submit(function (e) {
    $('input[name*="items-"]:disabled').each(function (e) {
      $(this).removeAttr('disabled');
    });
  });

  $('#startButton').on("click", startTimeCounter);
  $('#stopButton').on("click", stopTimeCounter);

  var t;
  function startTimeCounter() {
    $('#startButton').addClass('d-none');
    $('#submitButton').attr('disabled', true);
    $('#stopButton').removeClass('d-none');
    $('#id_duration').attr('readonly', true);

    var startTime = Math.floor(Date.now() / 1000);

    t = setInterval(function () {
      var diff = Math.floor(Date.now() / 1000) - startTime;
      var h = formatTime(Math.floor(diff / 3600));
      var m = formatTime(Math.floor(diff % 3600 / 60));
      document.getElementById("id_duration").value = h + ":" + m;
    }, 500);
  }

  function stopTimeCounter() {
    $('#stopButton').addClass('d-none');
    $('#startButton').removeClass('d-none');
    $('#submitButton').attr('disabled', false);
    $('#id_duration').attr('readonly', false);
    clearInterval(t);
  }

  $('[data-toggle="tooltip"]').tooltip()

  function detectswipe(el) {
    swipe_det = new Object();
    swipe_det.sX = 0; swipe_det.sY = 0; swipe_det.eX = 0; swipe_det.eY = 0;
    var min_x = 30;  //min x swipe for horizontal swipe
    var max_x = 30;  //max x difference for vertical swipe
    var min_y = 50;  //min y swipe for vertical swipe
    var max_y = 60;  //max y difference for horizontal swipe
    var direc = "";
    ele = document.getElementById(el);
    ele.addEventListener('touchstart', function (e) {
      var t = e.touches[0];
      swipe_det.sX = t.screenX;
      swipe_det.sY = t.screenY;
    }, false);
    ele.addEventListener('touchmove', function (e) {
      e.preventDefault();
      var t = e.touches[0];
      swipe_det.eX = t.screenX;
      swipe_det.eY = t.screenY;
    }, false);
    ele.addEventListener('touchend', function (e) {
      //horizontal detection
      if ((((swipe_det.eX - min_x > swipe_det.sX) || (swipe_det.eX + min_x < swipe_det.sX)) && ((swipe_det.eY < swipe_det.sY + max_y) && (swipe_det.sY > swipe_det.eY - max_y) && (swipe_det.eX > 0)))) {
        if (swipe_det.eX > swipe_det.sX) {
          direc = "r";
        }
        else {
          direc = "l";
        }
      }

      if (direc == 'r' && !$('.sidenav-button').hasClass("sidenav-button-show")) {
        toggleSidebar();
      }
      else if (direc == 'l' && $('.sidenav-button').hasClass("sidenav-button-show")) {
        toggleSidebar();
      }
      direc = "";
      swipe_det.sX = 0; swipe_det.sY = 0; swipe_det.eX = 0; swipe_det.eY = 0;
    }, false);
  }

  detectswipe('chev');
});

function updateInputs() {
  if ($('#id_hosting_provider option:selected').text() == "Other") {
    $('#div_id_provider_id').addClass('d-none');
    $('#div_id_label').removeClass('d-none');
    $('#div_id_operating_system').removeClass('d-none');
    $('#div_id_specifications').removeClass('d-none');
    $('#div_id_ip_address').removeClass('d-none');
    $('#div_id_price').removeClass('d-none');
    $('#div_id_region').removeClass('d-none');
  }
  else {
    $('#div_id_provider_id').removeClass('d-none');
    $('#div_id_label').addClass('d-none');
    $('#div_id_operating_system').addClass('d-none');
    $('#div_id_specifications').addClass('d-none');
    $('#div_id_ip_address').addClass('d-none');
    $('#div_id_price').addClass('d-none');
    $('#div_id_region').addClass('d-none');
  }
}

function updateItem(e) {
  number = e.target.name.match(/\d+/);

  var hours = 'input[name="items-' + number + '-hours"]';
  var rate = 'input[name="items-' + number + '-rate"]';
  var amount = 'input[name="items-' + number + '-amount"]';


  if ($(hours).val() || $(rate).val()) {
    $(amount).prop('disabled', true);
    if ($(hours).val() && $(rate).val()) {
      $(amount).val(($(hours).val() * $(rate).val()).toFixed(2)).trigger('change');
    }
    else {
      $(amount).val("").trigger('change');
    }
  }
  else if ($(amount).val()) {
    $(rate).prop('disabled', true);
    $(hours).prop('disabled', true);
  }
  else {
    $(rate).prop('disabled', false);
    $(hours).prop('disabled', false);
    $(amount).prop('disabled', false);
  }
}

function toggleSidebar() {
  $('.sidenav').toggleClass("sidenav-show");
  $('.main').toggleClass("main-below");
  $('body').toggleClass("body-below");
  $('.sidenav-button').toggleClass("sidenav-button-show");
}

function formatTime(i) {
  if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
  return i;
}
