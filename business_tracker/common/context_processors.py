from django.db.models import Q
from apps.accounts.models import Shift
from apps.invoices.models import Invoice


def dashboard(request):
    if request.path == "/":
        shifts = Shift.objects.filter(worker=request.user).filter(project=None)
        if request.user.is_superuser:
            invoices = Invoice.objects.filter(Q(status=2) | Q(status=0) | Q(status=1))
            return {"shifts": shifts, "invoices": invoices}
        return {"shifts": shifts}
    return {}
