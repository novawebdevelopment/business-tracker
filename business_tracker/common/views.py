import json
import os
from io import StringIO
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.management import call_command
from django.views import View
from django.contrib import messages
from django.shortcuts import render
from django.conf import settings
from common.forms import SettingsForm
from common.mixins import PermissionsRequiredMixin, NextPageMixin


class SettingsView(PermissionsRequiredMixin, View):
    superuser = True

    def get(self, request):
        form = SettingsForm()
        return render(request=request, template_name="settings.html", context={"form": form})

    def post(self, request):
        form = SettingsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            new_template = request.FILES.get("invoice_template", None)
            if new_template:
                with open(settings.TEMPLATE_FILE, "wb+") as destination:
                    for chunk in new_template.chunks():
                        destination.write(chunk)
            messages.add_message(request, messages.SUCCESS, "The settings have been sucessfully saved.")
            form = SettingsForm()
        return render(request=request, template_name="settings.html", context={"form": form})


class DumpJsonView(PermissionsRequiredMixin, View):
    superuser = True

    def get(self, request):
        output = StringIO()
        call_command("dumpdata", format="json", exclude=["auth", "contenttypes", "sessions"], stdout=output)
        return JsonResponse(json.loads(output.getvalue()), safe=False)


class DumpXmlView(PermissionsRequiredMixin, View):
    superuser = True

    def get(self, request):
        output = StringIO()
        call_command("dumpdata", format="xml", exclude=["auth", "contenttypes", "sessions"], stdout=output)
        return HttpResponse(output.getvalue(), content_type="text/xml")


class ResetInvoiceTemplateView(PermissionsRequiredMixin, NextPageMixin, View):
    superuser = True

    def get(self, request):
        default_template = os.path.join(settings.BASE_DIR + "/apps/invoices/templates/invoices/pdfs/default_template.pdf")
        with open(default_template, "rb") as input_file:
            with open(settings.TEMPLATE_FILE, "wb+") as output_file:
                output_file.write(input_file.read())
        messages.add_message(request, messages.SUCCESS, "The invoice template have been sucessfully reset.")
        return HttpResponseRedirect(self.next)