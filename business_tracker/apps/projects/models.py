import datetime
from decimal import Decimal
from django.db import models
from django.utils.html import mark_safe
from apps.partners.models import Partner


class Project(models.Model):
    uid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True)
    due_date = models.DateField(blank=True, null=True, help_text="Can be left empty")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    @property
    def is_due(self):
        if self.due_date is not None and datetime.date.today() > self.due_date:
            return True
        return False

    def get_balance(self):
        total = Decimal("0.00")
        return total

    def get_tracked_time(self):
        time = 0
        for shift in self.shifts.all():
            time += shift.duration
        hours = time // 3600
        minutes = time % 3600 // 60
        return f"{hours:02d}:{minutes:02d}"
