from django.urls import path
from apps.projects import views

app_name = "projects"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("<int:uid>/", views.DetailView.as_view(), name="detail"),
    path("<int:uid>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:uid>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<int:uid>/generate/ros/", views.GenerateROSView.as_view(), name="generate_ros"),
]
