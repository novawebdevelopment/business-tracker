import datetime
from django.views import View
from django.forms import ValidationError
from django.shortcuts import render, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from common.mixins import AccessModelMixin, PermissionsRequiredMixin, NextPageMixin
from apps.projects.models import Project
from apps.projects.forms import ProjectForm, GenerateROSForm


class ListView(PermissionsRequiredMixin, View):
    def get(self, request):
        projects = Project.objects.all()
        return render(request=request, template_name="projects/list.html", context={"projects": projects})


class CreateView(PermissionsRequiredMixin, NextPageMixin, View):
    superuser = True

    def get(self, request):
        form = ProjectForm()
        return render(request=request, template_name="projects/create.html", context={"form": form})

    def post(self, request):
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "The project has been successfully created.")
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="projects/create.html", context={"form": form})


class DetailView(PermissionsRequiredMixin, AccessModelMixin, View):
    model = Project

    def get(self, request):
        dates = request.session.get("dates", None)
        services = None
        if dates:
            services = (
                self.project.shifts.filter(date__gte=datetime.date.fromisoformat(dates["start_date"]))
                .filter(date__lte=datetime.date.fromisoformat(dates["end_date"]))
                .order_by("-date")
            )
            request.session.pop("dates")
        return render(
            request=request,
            template_name="projects/detail.html",
            context={"project": self.project, "services": services},
        )


class EditView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Project

    def get(self, request):
        form = ProjectForm(instance=self.project)
        return render(request=request, template_name="projects/edit.html", context={"form": form})

    def post(self, request):
        form = ProjectForm(request.POST, instance=self.project)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "The project has been successfully edited.")
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="projects/edit.html", context={"form": form})


class DeleteView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Project

    def get(self, request):
        return render(request=request, template_name="projects/delete.html")

    def post(self, request):
        self.project.delete()
        messages.add_message(request, messages.SUCCESS, "The project has been deleted.")
        return HttpResponseRedirect(self.next)


class GenerateROSView(PermissionsRequiredMixin, AccessModelMixin, View):
    model = Project

    def get(self, request):
        form = GenerateROSForm()
        return render(request=request, template_name="projects/generate/ros.html", context={"form": form})

    def post(self, request):
        form = GenerateROSForm(request.POST)
        if form.is_valid():
            dates = (
                self.project.shifts.filter(date__gte=form.cleaned_data["start_date"])
                .filter(date__lte=form.cleaned_data["end_date"])
                .order_by("-date")
                .count()
            )
            if dates:
                request.session["dates"] = {
                    "start_date": form.cleaned_data["start_date"].isoformat(),
                    "end_date": form.cleaned_data["end_date"].isoformat(),
                }
                return HttpResponseRedirect(reverse("projects:detail", kwargs={"uid": self.project.uid}))
            form.add_error(field=None, error=ValidationError("There is not data available in this interval."))
        return render(request=request, template_name="projects/generate/ros.html", context={"form": form})
