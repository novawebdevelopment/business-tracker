import datetime
from django import forms
from apps.projects.models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = "__all__"
        labels = {"due_date": "Due Date"}


class GenerateROSForm(forms.Form):
    start_date = forms.DateField(label="Start Date")
    end_date = forms.DateField(label="End Date")

    def clean(self):
        if self.errors:
            return
        if self.cleaned_data["start_date"] > self.cleaned_data["end_date"]:
            raise forms.ValidationError("The start date cannot be greater than the end date.")
        return self.cleaned_data
