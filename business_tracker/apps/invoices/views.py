from configparser import ConfigParser
from datetime import date
from smtplib import SMTPException
from django.views import View
from django.conf import settings
from django.shortcuts import render, reverse
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import EmailMessage
from django.core.mail.backends.smtp import EmailBackend
from common.mixins import AccessModelMixin, PermissionsRequiredMixin, NextPageMixin
from apps.invoices.models import Invoice, Item
from apps.invoices.forms import InvoiceForm, ItemForm
from apps.invoices.utils import generate_id, generate_pdf
from django.forms import inlineformset_factory


class ListView(PermissionsRequiredMixin, View):
    def get(self, request):
        invoices = Invoice.objects.all()
        paid_btn = {"active": "", "pressed": "", "href": "?show=1", "text": "Show"}
        void_btn = {"active": "", "pressed": "", "href": "?show=2", "text": "Show"}

        if request.GET.get("show", None) == "1":
            invoices = invoices.exclude(status=4)
            paid_btn = {"active": "active", "pressed": "true", "href": "", "text": "Hide"}
            void_btn["href"] = "?show=3"
        elif request.GET.get("show", None) == "2":
            invoices = invoices.exclude(status=3)
            void_btn = {"active": "active", "pressed": "true", "href": "", "text": "Hide"}
            paid_btn["href"] = "?show=3"
        elif request.GET.get("show", None) == "3":
            paid_btn = {"active": "active", "pressed": "true", "href": "?show=2", "text": "Hide"}
            void_btn = {"active": "active", "pressed": "true", "href": "?show=1", "text": "Hide"}
        else:
            invoices = invoices.exclude(status=3)
            invoices = invoices.exclude(status=4)

        return render(
            request=request,
            template_name="invoices/list.html",
            context={"invoices": invoices, "paid_btn": paid_btn, "void_btn": void_btn},
        )


class CreateView(PermissionsRequiredMixin, NextPageMixin, View):
    superuser = True

    def get(self, request):
        form = InvoiceForm()
        ItemsFormSet = inlineformset_factory(Invoice, Item, fields="__all__", extra=1)
        formset = ItemsFormSet()
        return render(request=request, template_name="invoices/create.html", context={"form": form, "formset": formset})

    def post(self, request):
        form = InvoiceForm(request.POST)
        ItemsFormSet = inlineformset_factory(Invoice, Item, fields="__all__", extra=0)
        if form.is_valid():
            invoice = form.save(commit=False)
            invoice.invoice_id = generate_id(form.cleaned_data["partner"])
            formset = ItemsFormSet(request.POST, instance=invoice)
            if formset.is_valid():
                invoice.save()
                form.save_m2m()
                formset.save()
                messages.add_message(request, messages.SUCCESS, "The invoice has been successfully created.")
                return HttpResponseRedirect(self.next)
        if request.POST.get("items-TOTAL_FORMS", "0") == "0":
            ItemsFormSet = inlineformset_factory(Invoice, Item, fields="__all__", extra=1)
            formset = ItemsFormSet()
        else:
            formset = ItemsFormSet(request.POST)
        return render(request=request, template_name="invoices/create.html", context={"form": form, "formset": formset})


class PDFView(PermissionsRequiredMixin, AccessModelMixin, View):
    model = Invoice

    def get(self, request):
        try:
            pdf = generate_pdf(self.invoice)
        except:
            messages.add_message(request, messages.ERROR, "The invoice template is not valid.")
            return HttpResponseRedirect(reverse("invoices:list"))
        if pdf is None:
            return HttpResponseRedirect(reverse("reset_invoice_template") + "?next=" + request.get_full_path())
        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = f'inline; filename="{self.invoice.invoice_id}.pdf"'
        return response


class EditView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Invoice

    def get(self, request):
        form = InvoiceForm(instance=self.invoice)
        if not self.invoice._can_delete:
            messages.add_message(request, messages.ERROR, "The invoice cannot be edited after it has been billed.")
            return HttpResponseRedirect(self.next)

        ItemsFormSet = inlineformset_factory(
            Invoice, Item, fields="__all__", extra=int(not bool(self.invoice.items.count()))
        )
        formset = ItemsFormSet(instance=self.invoice)
        return render(request=request, template_name="invoices/edit.html", context={"form": form, "formset": formset})

    def post(self, request):
        form = InvoiceForm(request.POST, instance=self.invoice)

        if not self.invoice._can_delete:
            messages.add_message(request, messages.ERROR, "The invoice cannot be edited after it has been billed.")
            return HttpResponseRedirect(self.next)

        ItemsFormSet = inlineformset_factory(Invoice, Item, fields="__all__", extra=0)
        formset = ItemsFormSet(request.POST, instance=self.invoice)
        if form.is_valid() and formset.is_valid():
            invoice = form.save()
            if invoice.status == 1 and not invoice.bill_date:
                invoice.bill_date = date.today()
                invoice.save()
            formset.save()
            messages.add_message(request, messages.SUCCESS, "The invoice has been successfully edited.")
            return HttpResponseRedirect(self.next)
        if request.POST.get("items-TOTAL_FORMS", "0") == "0":
            ItemsFormSet = inlineformset_factory(Invoice, Item, fields="__all__", extra=1)
            formset = ItemsFormSet()
        return render(request=request, template_name="invoices/edit.html", context={"form": form, "formset": formset})


class DeleteView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Invoice

    def get(self, request):
        return render(
            request=request, template_name="invoices/delete.html", context={"can_delete": self.invoice._can_delete}
        )

    def post(self, request):
        if not self.invoice._can_delete:
            self.invoice.status = 4
            self.invoice.save()
            messages.add_message(request, messages.SUCCESS, "The invoice has been voided.")
        else:
            self.invoice.delete()
            messages.add_message(request, messages.SUCCESS, "The invoice has been deleted.")
        return HttpResponseRedirect(self.next)


class AdvanceView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Invoice

    def get(self, request):
        config = ConfigParser(interpolation=None)
        config.read(settings.CONFIG_FILE)

        if self.invoice.status == 0:
            if config.getboolean("invoices", "AUTO_EMAIL"):
                self.invoice.status = 1
                self.invoice.bill_date = date.today()
                pdf = generate_pdf(self.invoice)
                if not pdf:
                    messages.add_message(request, messages.ERROR, "The invoice template is not valid.")
                    return HttpResponseRedirect(self.next)

                msg = config.get("invoices", "BILLING_MESSAGE").replace(
                    "[contact_name]", self.invoice.partner.contact_name
                )

                backend = EmailBackend(
                    host=config.get("email", "EMAIL_HOST"),
                    port=config.getint("email", "EMAIL_PORT"),
                    username=config.get("email", "EMAIL_USER"),
                    password=config.get("email", "EMAIL_PASSWORD"),
                    use_tls=config.getboolean("email", "EMAIL_USE_TLS"),
                )
                try:
                    email = EmailMessage(
                        subject="Invoice " + self.invoice.invoice_id,
                        body=msg,
                        from_email=config.get("email", "EMAIL_USER"),
                        to=(self.invoice.partner.contact_email,),
                        connection=backend,
                        attachments=((f"{self.invoice.invoice_id}.pdf", pdf, "application/pdf"),),
                    )
                    email.send()
                except (ConnectionError, OSError, SMTPException):
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "The settings for sending email are not valid.",
                    )
                    return HttpResponseRedirect(self.next)
                self.invoice.status = 1
                self.invoice.bill_date = date.today()
                self.invoice.save()
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    f"The invoice has been billed and sent to {self.invoice.partner.contact_email}.",
                )
            else:
                self.invoice.status = 1
                self.invoice.bill_date = date.today()
                self.invoice.save()
                messages.add_message(request, messages.SUCCESS, "The invoice has been billed.")
            return HttpResponseRedirect(self.next)
        elif self.invoice.status == 1 or self.invoice.status == 2:
            self.invoice.status = 3
            self.invoice.save()
            messages.add_message(request, messages.SUCCESS, "The invoice has been paid.")
            return HttpResponseRedirect(self.next)
        messages.add_message(request, messages.ERROR, "The invoice status is final.")
        return HttpResponseRedirect(self.next)
