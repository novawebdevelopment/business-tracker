from django import forms
from django.conf import settings
from configparser import ConfigParser
from apps.invoices.models import Invoice, Item


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ("partner", "notice")
        help_texts = {
            "notice": "Displayed at the bottom of the invoice. Long text might be cut out depending on the invoice template"
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        config = ConfigParser(interpolation=None)
        config.read(settings.CONFIG_FILE)

        self.fields["partner"].empty_label = ""
        self.fields["notice"].initial = config.get("invoices", "NOTICE")
        self.instance = getattr(self, "instance", None)
        if self.instance and self.instance.pk:
            self.fields["partner"].disabled = True


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = "__all__"
