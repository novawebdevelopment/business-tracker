import os
import tempfile
import pypdftk
from datetime import date
from io import BytesIO
from django.conf import settings
from apps.invoices.models import Invoice


def generate_id(partner):
    invoice_id = str(date.today().year) + "-"
    invoice_id += partner.partner_id + "-"
    invoices = []
    for invoice in Invoice.objects.filter(partner=partner):
        if invoice.invoice_id[:4] == str(date.today().year):
            invoices.append(int(invoice.invoice_id[-3:]))
    if len(invoices) > 0:
        invoices.sort()
        invoice_id += f"{(invoices[-1] + 1):03}"
    else:
        invoice_id += "001"
    return invoice_id


def generate_pdf(invoice):
    if not os.path.isfile(settings.TEMPLATE_FILE):
        return None

    invoice_date = ""
    items = ""
    hours = ""
    rate = ""
    amount = ""

    if invoice.bill_date:
        invoice_date = invoice.bill_date.strftime("%m/%d/%Y")

    for item in invoice.items.all():
        items = items + item.description + "\n"

        if item.hours:
            hours += str(item.hours) + "\n"
            rate += "$" + f"{item.rate:,.2f}" + "\n"
        else:
            hours += "\n"
            rate += "\n"

        amount += "$" + f"{item.amount:,.2f}" + "\n"

    invoice_data = {
        "INVOICE_ID": invoice.invoice_id,
        "BILL_DATE": invoice_date,
        "PARTNER": invoice.partner.company,
        "ADDRESS1": invoice.partner.get_address1(),
        "ADDRESS2": invoice.partner.get_address2(),
        "ITEMS": items,
        "HOURS": hours,
        "RATE": rate,
        "AMOUNT": amount,
        "TOTAL": "$" + f"{invoice.get_total():,.2f}",
        "NOTICE": invoice.notice,
    }

    pdf_invoice = tempfile.NamedTemporaryFile()
    pypdftk.fill_form(settings.TEMPLATE_FILE, invoice_data, pdf_invoice.name, flatten=True)

    if invoice.status != 1:
        pdf_invoice_watermark = tempfile.NamedTemporaryFile()
        path = os.path.join(settings.BASE_DIR + "/apps/invoices/templates/invoices/pdfs/")
        pypdftk.stamp(pdf_invoice.name, path + str(invoice.status) + ".pdf", pdf_invoice_watermark.name)
        stream = BytesIO(pdf_invoice_watermark.read())
        pdf_invoice.close()
        pdf_invoice_watermark.close()
    else:
        stream = BytesIO(pdf_invoice.read())
        pdf_invoice.close()

    return stream.getvalue()
