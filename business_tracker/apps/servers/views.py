from django.views import View
from django.shortcuts import render, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.forms import inlineformset_factory
from common.mixins import AccessModelMixin, PermissionsRequiredMixin, NextPageMixin
from apps.servers import apis
from apps.servers.models import Server, Service
from apps.servers.forms import ServerForm, ServiceForm


class ListView(PermissionsRequiredMixin, View):
    def get(self, request):
        servers = Server.objects.all()
        for server in servers:
            server.status_color = apis.get_status_color(server)
        return render(request=request, template_name="servers/list.html", context={"servers": servers})


class CreateView(PermissionsRequiredMixin, NextPageMixin, View):
    superuser = True

    def get(self, request):
        form = ServerForm()
        ServicesFormSet = inlineformset_factory(Server, Service, fields="__all__", extra=1)
        formset = ServicesFormSet()
        return render(request=request, template_name="servers/create.html", context={"form": form, "formset": formset})

    def post(self, request):
        form = ServerForm(request.POST)
        ServicesFormSet = inlineformset_factory(Server, Service, fields="__all__", extra=0)
        if form.is_valid():
            server = form.save(commit=False)
            formset = ServicesFormSet(request.POST, instance=server)
            if formset.is_valid():
                server.save()
                form.save_m2m()
                formset.save()
                if server.hosting_provider != Server.OTHER:
                    apis.update_servers()
                messages.add_message(request, messages.SUCCESS, "The server has been successfully created.")
                return HttpResponseRedirect(self.next)
        if request.POST.get("services-TOTAL_FORMS", "0") == "0":
            ServicesFormSet = inlineformset_factory(Server, Service, fields="__all__", extra=1)
            formset = ServicesFormSet()
        else:
            formset = ServicesFormSet(request.POST)
        return render(request=request, template_name="servers/create.html", context={"form": form, "formset": formset})


class DetailView(PermissionsRequiredMixin, AccessModelMixin, View):
    model = Server

    def get(self, request):
        self.server.status_color = apis.get_status_color(self.server)
        return render(request=request, template_name="servers/detail.html", context={"server": self.server})


class EditView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Server

    def get(self, request):
        form = ServerForm(instance=self.server)
        ServicesFormSet = inlineformset_factory(
            Server, Service, fields="__all__", extra=int(not bool(self.server.services.count()))
        )
        formset = ServicesFormSet(instance=self.server)
        return render(request=request, template_name="servers/edit.html", context={"form": form, "formset": formset})

    def post(self, request):
        form = ServerForm(request.POST, instance=self.server)
        ServicesFormSet = inlineformset_factory(Server, Service, fields="__all__", extra=0)
        formset = ServicesFormSet(request.POST, instance=self.server)
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            messages.add_message(request, messages.SUCCESS, "The server has been successfully edited.")
            return HttpResponseRedirect(self.next)
        if request.POST.get("services-TOTAL_FORMS", "0") == "0":
            ServicesFormSet = inlineformset_factory(Server, Service, fields="__all__", extra=1)
            formset = ServicesFormSet(instance=self.server)
        return render(request=request, template_name="servers/edit.html", context={"form": form, "formset": formset})


class DeleteView(PermissionsRequiredMixin, AccessModelMixin, NextPageMixin, View):
    superuser = True
    model = Server

    def get(self, request):
        return render(request=request, template_name="servers/delete.html")

    def post(self, request):
        self.server.delete()
        messages.add_message(request, messages.SUCCESS, "The server has been deleted.")
        return HttpResponseRedirect(self.next)
