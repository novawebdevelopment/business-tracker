from django import forms
from django.conf import settings
from apps.servers import apis
from apps.servers.models import Server, Service


class ServerForm(forms.ModelForm):
    hosting_provider = forms.ChoiceField(choices=apis.get_hosting_providers)

    class Meta:
        model = Server
        exclude = ("status",)
        labels = {
            "hosting_provider": "Hosting Provider",
            "provider_id": "Provider ID",
            "operating_system": "Operating System",
            "ip_address": "IP Address",
            "root_password": "Root Password",
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.instance = getattr(self, "instance", None)
        fields_to_del = set()

        if self.instance and self.instance.pk:
            self.fields["hosting_provider"].disabled = True
            self.fields["provider_id"].disabled = True
            if self.instance.hosting_provider == Server.OTHER:
                fields_to_del.add("provider_id")
            else:
                fields_to_del.add("label")
                fields_to_del.add("operating_system")
                fields_to_del.add("specifications")
                fields_to_del.add("ip_address")
                fields_to_del.add("price")
                fields_to_del.add("region")
        elif self.data.get("hosting_provider", None):
            if int(self.data.get("hosting_provider", None)) == Server.OTHER:
                self.fields["provider_id"].required = False
            else:
                self.fields["label"].required = False
                self.fields["operating_system"].required = False
                self.fields["specifications"].required = False
                self.fields["ip_address"].required = False
                self.fields["price"].required = False
                self.fields["region"].required = False

        for field in fields_to_del:
            del self.fields[field]

    def clean(self):
        self.cleaned_data = super().clean()
        if self.errors:
            return
        if int(self.cleaned_data["hosting_provider"]) == Server.OTHER:
            self.cleaned_data["provider_id"] = ""
        if not apis.is_server_id_valid(self.cleaned_data["provider_id"], int(self.cleaned_data["hosting_provider"])):
            raise forms.ValidationError("The Provider ID is not valid.")
        return self.cleaned_data


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = "__all__"
