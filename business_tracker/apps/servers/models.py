from decimal import Decimal
from django.db import models


class Server(models.Model):
    LINODE = 0
    OTHER = 9

    HOSTING_PROVIDERS = ((LINODE, "Linode"), (OTHER, "Other"))

    uid = models.AutoField(primary_key=True)
    hosting_provider = models.PositiveSmallIntegerField()
    provider_id = models.CharField(max_length=255, blank=False)

    label = models.CharField(max_length=64, blank=False)
    operating_system = models.CharField(max_length=32, blank=False)
    specifications = models.TextField(blank=False)
    ip_address = models.CharField(max_length=40, blank=False)
    root_password = models.CharField(max_length=64)
    price = models.CharField(max_length=16, blank=False)

    status = models.CharField(max_length=32, blank=False, default="N/A")
    region = models.CharField(max_length=32, blank=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_hosting_provider(self):
        return dict(self.HOSTING_PROVIDERS).get(self.hosting_provider)


class Service(models.Model):
    uid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    platform = models.CharField(max_length=32)
    url = models.URLField()
    server = models.ForeignKey(Server, related_name="services", on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["created_at"]
