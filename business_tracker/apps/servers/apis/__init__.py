from apps.servers.apis import linode
from apps.servers.models import Server


def get_hosting_providers():
    hosting_providers = list(Server.HOSTING_PROVIDERS)
    if not linode.is_api_token_valid():
        hosting_providers.remove((Server.LINODE, "Linode"))
    return hosting_providers


def is_server_id_valid(id, hosting_provider):
    if hosting_provider == Server.LINODE:
        return linode.is_server_id_valid(id)
    return True


def get_status_color(server):
    if server.status != "N/A":
        if server.hosting_provider == Server.LINODE:
            return linode.STATUS_COLORS[server.status]
    return "dark"


def update_servers():
    msg = f"Updated {linode.update_servers()} Linodes."
    return msg
