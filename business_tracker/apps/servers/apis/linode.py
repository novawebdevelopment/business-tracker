from configparser import ConfigParser
from linode_api4 import LinodeClient
from linode_api4.errors import ApiError
from linode_api4.objects.linode import Instance
from django.conf import settings
from apps.servers.models import Server

config = ConfigParser(interpolation=None)

STATUS_COLORS = {
    "running": "success",
    "offline": "danger",
    "booting": "primary",
    "rebooting": "careful",
    "shutting_down": "danger",
    "provisioning": "primary",
    "deleting": "danger",
    "migrating": "warning",
    "rebuilding": "careful",
    "cloning": "warning",
    "restoring": "warning",
    "stopped": "danger",
}


def is_api_token_valid():
    try:
        config.read(settings.CONFIG_FILE)
        client = LinodeClient(config.get("servers", "LINODE_API_TOKEN"))
        linodes = client.linode.instances()
        return True
    except (ApiError, RuntimeError):
        return False


def is_server_id_valid(id):
    try:
        config.read(settings.CONFIG_FILE)
        client = LinodeClient(config.get("servers", "LINODE_API_TOKEN"))
        if len(client.linode.instances(Instance.id == id)) > 0:
            return True
        return False
    except ApiError:
        return False


def update_servers():
    config.read(settings.CONFIG_FILE)
    count = 0
    try:
        client = LinodeClient(config.get("servers", "LINODE_API_TOKEN"))
        linodes = client.linode.instances()
    except ApiError:
        print("Invalid Linode API Token.")
        return count
    for server in Server.objects.filter(hosting_provider=Server.LINODE):
        for linode in linodes:
            if server.provider_id == str(linode.id):
                server.label = linode.label
                server.operating_system = linode.image.label
                server.specifications = linode.type.label
                server.ip_address = linode.ipv4[0]
                server.price = "${:,.2f}/mo".format(linode.type.price.monthly)
                server.status = linode.status
                server.region = linode.region.id
                server.save()
                count += 1
    return count
